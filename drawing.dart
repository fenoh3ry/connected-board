import 'package:flutter/material.dart';

class Drawing extends CustomPainter {
  // Création du stylo
  List<Offset> points;
  Drawing(this.points);
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = new Paint()
      ..color = Colors.black
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 5.0;

    for (int i = 0; i < points.length; i++) {
      if (points[i] != null && points[i + 1] != null) {
        canvas.drawLine(points[i], points[i + 1], paint);
      }
    }
  }

  @override
  bool shouldRepaint(Drawing oldDelegate) => oldDelegate.points != points;
}
