import 'package:connectedboard/src/views/screens/draw.screen.dart';
import 'package:connectedboard/src/views/screens/view.screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_socket_io/flutter_socket_io.dart';
import 'package:flutter_socket_io/socket_io_manager.dart';

class MenuScreen extends StatefulWidget {
  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {
  SocketIO socketIO;

  void init() {
    socketIO =
        SocketIOManager().createSocketIO("https://d11db1e2d27f.ngrok.io", "/");
    socketIO.init();
    socketIO.connect();
  }

  @override
  void initState() {
    super.initState();
    init();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RaisedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (_) => DrawScreen(socketIO)));
                  },
                  child: Text("Ecrire un message"),
                ),
                RaisedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (_) => ViewScreen(socketIO)));
                  },
                  child: Text("Regarder le message"),
                ),
              ],
            )));
  }
}
