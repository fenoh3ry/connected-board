import 'package:connectedboard/src/views/widgets/drawing.dart';
import 'package:flutter/material.dart';
import 'package:flutter_socket_io/flutter_socket_io.dart';

class DrawScreen extends StatefulWidget {
  final SocketIO socket;
  DrawScreen(this.socket);
  @override
  _DrawScreenState createState() => _DrawScreenState();
}

class _DrawScreenState extends State<DrawScreen> {
  List<Offset> _points = <Offset>[];

  void sendPoints(List<Offset> p) {
    List<String> liste = [];
    p
        .map((e) => e != null
            ? liste.add("${e.dx},${e.dy}")
            : liste.add(
                null)) //prendre chaque coordonnées pour les transformer en tableau de coordonnées
        .toList();

    String jsonData =
        '{"data":"${liste.join(";")}"}'; // Envoyer les datas sous forme de string (question de préférence)

    widget.socket.sendMessage("get_drawing", jsonData);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(
          children: [
            GestureDetector(
              onPanUpdate: (DragUpdateDetails details) {
                setState(() {
                  _points = new List.from(_points)..add(details.globalPosition);
                });
              },
              onPanEnd: (DragEndDetails details) {
                _points.add(null);
                sendPoints(_points);
              },
              child:
                  CustomPaint(painter: Drawing(_points), size: Size.infinite),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 48),
              child: IconButton(
                  icon: Icon(Icons.delete_outline),
                  onPressed: () {
                    setState(() {
                      _points.clear();
                    });
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
