import 'dart:convert';

import 'package:connectedboard/src/views/widgets/drawing.dart';
import 'package:flutter/material.dart';
import 'package:flutter_socket_io/flutter_socket_io.dart';

class ViewScreen extends StatefulWidget {
  final SocketIO socket;
  ViewScreen(this.socket);
  @override
  _ViewScreenState createState() => _ViewScreenState();
}

class _ViewScreenState extends State<ViewScreen> {
  List<Offset> _points =
      <Offset>[]; // Liste des coordonées qui vont déterminer le dessin

  @override
  void initState() {
    super.initState();
    widget.socket.subscribe("draw", (data) {
      // Ecouter l'évènement lorsque l'utilisateur écrit
      List<String> got =
          json.decode(data)["data"].split(";"); //données reçus du serveur
      List<Offset> offsets = []; // initialiser un tableau de points
      got.map((e) {
        if (e != "null") {
          List<String> yn = e.split(",");
          offsets.add(Offset(
              double.parse(yn[0]),
              double.parse(
                  yn[1]))); // prendre chaque coordonnées pour créer le point
        } else {
          offsets.add(null); // permet de ne pas avoir de dessin continu
        }
      }).toList();

      setState(() {
        _points =
            offsets; // passer la liste des points au variable _points pour l'afficher
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(
          children: [
            Container(
              child:
                  CustomPaint(painter: Drawing(_points), size: Size.infinite),
            ),
          ],
        ),
      ),
    );
  }
}
